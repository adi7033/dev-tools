/**
 *	app.js
 *
 *	@file This file is the starting point of your application
 *
 *	@project $(SLUSH_PROJECT_NAME)
 *	@author $(SLUSH_AUTHOR) $(SLUSH_EMAIL)
 *	@date $(SLUSH_DATE)
 *
 *	The current project was auto generated with Slush Dev Tool
 *	@author Chad Vanet <chad.vanet@coatesgroup.com>
 *
 */

$(function() {
	/**
	 *	@constant
	 *	@type {string}
	 *	@default - (required for local development)
	 */
	const hq = SB.local ? '$(SLUCH_HQ)' : window.location.protocol+'//'+window.location.host+'/';
	/**
	 *	@constant
	 *	@type {string}
	 *	@default - 'horizontal' | 'vertical' (required for local development)
	 */
	const orientation = SB.local ? '$(SLUCH_ORIENTATION)' : SB.Env.orientation;

	/**
	 *	@constant
	 *	@type {string}
	 *	@default - Location code (required for local development)
	 */
	const devLocationCode = 'LOCATION_CODE';
	/**
	 *	@constant
	 *	@type {int}
	 *	@default - Channel screen ID (required for local development)
	 */
	const devChannelScreenId = 1;

	/**
	 *	Event triggered to deactivate any animations currently running
	 *
	 *	@function window.deactivate
	 *	@property {Boolean} window.activated - @default false
	 *	@return {Boolean} window.activated
	 */
	window.deactivate = function() {
		window.activated = false;

		return window.activated;
	};
	/**
	 *	Event triggered when activating content. Trigger animations
	 *
	 *	@function window.activate
	 *	@property {Boolean} window.activated - @default true
	 *	@return {Boolean} window.activated
	 */
	window.activate = function() {
		window.activated = true;

		return window.activated;
	};
	/**
	 *	Event triggered when getting content ready before activation. Position / prepare animations behind the scene
	 *
	 *	@function window.prepare
	 *	@return {Boolean} - @default true
	 */
	window.prepare = function() {

		return true;
	};

	/**
	 *	This function is the starting point of our application
	 *
	 *	@function init
	 *	@callback SB.setup
	 */
	function init() {

	}

	/**
	 *
	 *	@property {String}  	url					- Switchboard HQ (required for local environment only)
	 *	@property {Function}	success				- Callback function
	 *	@property {String}		environmentLocation	- Environment Location (required for local environment only)
	 *	@property {String}		content				- Name of the content (required for local environment only)
	 *	@property {Object}		sources				- Datasources Object
	 *	@property {String}		sources.filename	- Name of the Datasource File ("example-name.csv")
	 *	@property {String}		sources.name		- Handy name for the datasource (used to retrieve the datasource matching "filename")
	 */
	SB.setup({
		url: hq,
		environmentLocation: devLocationCode,
		channelScreenId: devChannelScreenId,
		content: getContentName(),
		sources: [
			{ name: 'my_datasource', filename: 'my_datasource.csv'},
		],
	}).then(function() {
		init();
	});
});