/**
 *	app.js
 *
 *	@file Thisis the starting point of your application
 *
 *	@project $(SLUSH_PROJECT_NAME)
 *	@author $(SLUSH_AUTHOR) $(SLUSH_EMAIL)
 *	@date $(SLUSH_DATE)
 *
 *	The current project was auto generated with Slush Dev Tool
 *	@author Chad Vanet <chad.vanet@coatesgroup.com>
 *
 */

$(function() {
	/**
	 *	@constant
	 *	@type {string}
	 *	@default - 'horizontal' | 'vertical' (required for local development)
	 */
	const orientation = '$(SLUCH_ORIENTATION)';


	/**
	 *	Event triggered to deactivate any animations currently running
	 *
	 *	@function window.deactivate
	 *	@property {Boolean} window.activated - @default false
	 *	@return {Boolean} window.activated
	 */
	window.deactivate = function() {
		window.activated = false;

		return window.activated;
	};
	/**
	 *	Event triggered when activating content. Trigger animations
	 *
	 *	@function window.activate
	 *	@property {Boolean} window.activated - @default true
	 *	@return {Boolean} window.activated
	 */
	window.activate = function() {
		window.activated = true;

		return window.activated;
	};
	/**
	 *	Event triggered when getting content ready before activation. Position / prepare animations behind the scene
	 *
	 *	@function window.prepare
	 *	@return {Boolean} - @default true
	 */
	window.prepare = function() {

		return true;
	};


	/**
	 *	This function is the starting point of our application
	 *
	 *	@function init
	 */
	function init() {
		// Statements
		
	}

	init();
});