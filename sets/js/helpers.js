/**
 *	helpers.js
 *
 *	@file Includes generic utilities and helper functions
 *	@author Chad Vanet <chad.vanet@coatesgroup.com>
 *	@copyright Coates Technology 2016
 */

/**
 *	Get name of current content from URL
 *
 *	@function getContentName
 *	@return {String}
 */
function getContentName() {
	return _.find( $(location).attr('href').split('/'), function(el) {
		return el.search('.zip') > 0;
	}).replace('%20', ' ');
}

/**
 *	Deeply transcript property names to Camel Case
 *
 *	@function camelCaseObj
 *	@param {Object} obj - Any object or array
 *	@return {Object}
 */
function camelCaseObj(obj) {
	if (!Array.isArray(obj) && typeof obj !== 'object') return obj;
	return Object.keys(obj).reduce(function(acc, key) {
		acc[_.camelCase(key)] = typeof obj[key] === 'string' ? obj[key] : camelCaseObj(obj[key]);
		return acc;
	}, Array.isArray(obj) ? [] : {});
}

/**
 *	Return filename extension or filename if no extension
 *
 *	@function getExtension
 *	@param {String} filename
 *	@return {String}
 */
function getExtension(filename) {
	return filename ? filename.split('.').pop() : '';
}

/**
 *	Check if provided file name is an image file supported by Switchboard
 *
 *	@function isImage
 *	@param {String} filename
 *	@return {Boolean}
 */
function isImage(filename) {
	var ext = getExtension(filename);
	if (ext) {
		ext = ext.toLowerCase();
		if (ext === 'jpg' || ext === 'jpeg' || ext === 'png') {
			return true;
		}
	}
	return false;
}

