$(function() {
	function refreshPanel(that) {
		var target = $(that).data('panel');
		$('#feature_panel').addClass('bounceInRight');
		$('.feature').removeClass('feature-active');
		$('#'+target).addClass('feature-active');
		$('#panel_title').html($(that).text());
		setTimeout(function() {
			$('#feature_panel').removeClass('bounceInRight');
		}, 500);

	}
	$(".nav a").on("click", function() {
		$(".nav").find(".active").removeClass("active");
		$(this).parent().addClass("active");
		if (!$('*[data-subnav="commands"]').parent().hasClass('active') && !$(this).hasClass('subnav-link')) {
			$('#nav_commands').addClass('collapse');
		}
		refreshPanel(this);
	});

	$(".internal-link").on("click", function() {
		var menu ='*[data-panel="' + $(this).data('menu') + '"]';
		$(".nav").find(".active").removeClass("active");
		$(".nav").find(menu).parent().addClass("active");
		if (!$('*[data-subnav="commands"]').parent().hasClass('active') && !$(this).hasClass('subnav-link')) {
			$('#nav_commands').addClass('collapse');
		}
		refreshPanel(this);
	});

	$('*[data-subnav="commands"]').on("click", function() {
		$('#nav_commands').removeClass('collapse');
	});

	$('.subnav-link').on("click", function() {
		$($(this).data('target'))[0].scrollIntoView();
	});
});