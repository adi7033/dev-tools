const gulp 		= require('gulp'),
	sass 		= require('gulp-sass'),
	sassGlob 	= require('gulp-sass-glob'),
	minifyCSS 	= require('gulp-clean-css'),
	sourcemaps 	= require('gulp-sourcemaps'),
	browserSync = require('browser-sync'),
	del 		= require('del'),
	zip 		= require('gulp-zip'),
	rename 		= require('gulp-rename'),
	folders 	= require('gulp-folders'),
	path 		= require('path'),
	watch		= require('gulp-watch'),
	inquirer	= require('inquirer'),
	replace		= require('gulp-replace'),
	fs			= require('fs'),
	run			= require('gulp-run'),
	open		= require('open'),
	request		= require('request'),
	concat		= require('gulp-concat'),
	jsonfile	= require('jsonfile');


/**
* Paths settings
*/
const	sep					= path.sep,
		vendorPath			= path.normalize("src/vendor/"),
		piecescontentPath	= path.normalize("src/content/"),
		distPath			= path.normalize("dist/"),
		zippedPath			= path.normalize("zipped/"),
		datasourcesPath		= path.normalize("src/datasources/"),
		CLPath 				= vendorPath + path.normalize('content-library/');
		contentFolder 		= "content",
		fileFolder			= "file",
		previewsPath		= path.normalize("src/previews/");

/**
*	Javascript libraries
*/
const libs	= [
				{ lib: 'switchboard content framework [soda]', path: path.normalize('src/vendor/soda/Soda.js'), file: 'Soda.js', default: true },
				{ lib: 'jquery', path: path.normalize('src/vendor/jquery/dist/jquery.min.js'), file: 'jquery.min.js', default: true },
				{ lib: 'jquery-ui', path: path.normalize('src/vendor/jquery-ui/jquery-ui.min.js'), file: 'jquery-ui.min.js', default: true },
				{ lib: 'lodash', path: path.normalize('src/vendor/lodash/dist/lodash.min.js'), file: 'lodash.min.js', default: true },
				{ lib: 'moment.js (date and time library)', path: path.normalize('src/vendor/moment/min/moment.min.js'), file: 'moment.min.js', default: false },
				{ lib: 'switchboard content library', path: path.normalize('src/vendor/content-library/dist/sb.all.min.js'), file: 'sb.all.min.js', default: false },
				{ lib: 'handlebars.js (templating library)', path: path.normalize('src/vendor/handlebars/handlebars.min.js'), file: 'handlebars.min.js', default: false },
				{ lib: 'BBCode-parser.js (BBCode parser library)', path: path.normalize('src/vendor/extendible-bbcode-parser/xbbcode.js'), file: 'xbbcode.js', default: false },
				{ lib: 'socket-io-client.js (client socket lib for LMS)', path: path.normalize('src/vendor/socket.io-client/dist/socket.io.min.js'), file: 'socket.io.min.js', default: false }
			];

var BLUE = '\033[0;34m';
var CYAN = '\033[0;36m';
var RED = '\033[0;31m';
var GREEN = '\033[0;32m';
var NC = '\033[0m';

/**
 *	Helper functions
 */
function compileSass(vinyl, target) {
	var dest = vinyl.split(sep);
	dest = dest[dest.indexOf('content') + 1];
	return gulp.src(path.normalize(vinyl))
	.pipe(sourcemaps.init())
		.pipe(sassGlob())
		.pipe(sass().on('error', sass.logError))
		.pipe(minifyCSS())
	.pipe(sourcemaps.write())
	.pipe(rename( target ))
	.pipe(gulp.dest(path.join(distPath, dest, 'dist')))
	.pipe(browserSync.reload({
		stream: true,
	}));
}



/**
 *	Self updates
 */
function selfUpdate(callback) {
	return run('git submodule update --init --remote --force').exec(function() {
		if (process.platform === 'win32') {
			return run('copy /y .slush\\core\\install.sh . & copy /y .slush\\core\\bowerrc.json .bowerrc & copy /y .slush\\core\\bower.json . & copy /y .slush\\core\\package.json . & copy /y .slush\\core\\gulpfile.js . & robocopy /e /is .slush\\doc doc').exec(callback);
		} else {
			return run('cp .slush/core/install.sh . ; cp .slush/core/bowerrc.json .bowerrc ; cp .slush/core/bower.json . ; cp .slush/core/package.json . ; cp .slush/core/gulpfile.js . ; cp -rf .slush/doc . ; cp .slush/core/README.md .').exec(callback);
		}
	});
}
function updateLibs(callback) {
	return run('rm -rf ' + vendorPath + ' && mkdir ' + vendorPath + '; bower update -F').exec(callback);
}
gulp.task('update', function() {
	var questions = {
			type: 'confirm',
			name:'updateLibs',
			message: 'Check for Vendor Libraries updates?',
			default: false,
	};
	return inquirer.prompt(questions).then(function (answers) {
		request.get('https://gitlab.com/adi7033/dev-tools/raw/master/core/package.json', function (error, response, body) {
			if (!error && response.statusCode == 200) {
				console.log(GREEN + 'Connecting to remote... Please wait.' + NC);
				var originLibs = require('./bower');
				var originPackage = require('./package');
				var newPackage = JSON.parse(body);
				if (originPackage.version !== newPackage.version) {
					selfUpdate(function() {
						if (Object.keys(originPackage.devDependencies).length !== Object.keys(newPackage.devDependencies).length ||
							Object.keys(originPackage.dependencies).length !== Object.keys(newPackage.dependencies).length) {
							run('npm install').exec(function() {
								// if (!answers.updateLibs) {
								// 	console.log('Updated Dev Tools (v' + originPackage.version, '-> v' + newPackage.version + ')');
								// }
							});
						}
						if (answers.updateLibs) {
							updateLibs(function() { setTimeout(function() { console.log(GREEN+'Updated Libraries and Dev Tools (v' + originPackage.version, '-> v' + newPackage.version + ')'+NC); execCLBuild(); }, 500) });
						} else {
							var latestLibs = require('./.slush/core/bower');
							if (Object.keys(originLibs.dependencies).length !== Object.keys(latestLibs.dependencies).length) {
								run('bower install').exec(function() { setTimeout(function() { console.log('Updated Libraries and Dev Tools (v' + originPackage.version, '-> v' + newPackage.version + ')'); execCLBuild(); }, 500) });
							} else {
								setTimeout(function() { console.log(GREEN+'Updated Dev Tools (v' + originPackage.version, '-> v' + newPackage.version + ')'+NC) }, 500);
							}
						}
						return gulp
							.src('./.slush/vendor/Soda.js')
							.pipe(gulp.dest('src/vendor/soda/'))
					});
				} else {
					if (answers.updateLibs) {
						updateLibs(function() { setTimeout(function() { console.log(CYAN+'Dev Tools (v' + originPackage.version + ') and libraries are up to date'+NC); execCLBuild();  }, 500) });
					} else {
						console.log('Dev Tools up to date (v' + originPackage.version + ')');
					}
					return gulp
						.src('./.slush/vendor/Soda.js')
						.pipe(gulp.dest('src/vendor/soda/'))
				}
			} else {
				console.error(RED+'Error: No internet connection.'+NC);
			}
		});

	});
});
/**
*	Main tasks
*/
gulp.task('doc', function() {
	open(path.normalize('./.slush/doc/index.html'));
});
gulp.task('help', function() {
	open(path.normalize('./.slush/doc/index.html'));
});
gulp.task('version', function() {
	request.get('https://gitlab.com/adi7033/dev-tools/raw/master/core/package.json', function (error, response, body) {
		var update = require('./package');
		if (error) {
			console.log('Digital Production Dev Tools v' + update.version);
		} else {
			var latest = JSON.parse(body).version;
			if (update.version !== latest) {
				console.log('Digital Production Dev Tools ' + RED + 'v' + update.version + NC + '. Latest v' + GREEN + latest + NC);
				console.warn(CYAN + 'Please run the following command to update you dev environment:' + NC+'\n\tgulp update');
			} else {
				console.log('Digital Production Dev Tools ' + GREEN + 'v' + update.version + NC + '. Latest v' + GREEN + latest + NC);
			}
		}
	});
});
gulp.task('clean', function() {
	if (distPath !== '.' && distPath !== path.normalize('./') && zippedPath !== '.' && zippedPath !== path.normalize('./'))
		del.sync([distPath, zippedPath]);
});

gulp.task('check', function(cb) {
	var dirname = piecescontentPath;
	var files = fs.readdirSync(dirname);
	if (files.length === 0 || (files.length === 1 && files[0] === '.gitkeep') ) {
		console.error(RED+'Content bin (' + piecescontentPath + ') is empty. Nothing to build.'+NC);
		process.exit();
	}
	cb();
});
gulp.task('default', ['check', 'clean', 'serve'], function() {
	watch([
		path.join(piecescontentPath, '*.mp4'),
		path.join(piecescontentPath, '*.jpg'),
		path.join(piecescontentPath, '*.JPG'),
		path.join(piecescontentPath, '*.png'),
		path.join(piecescontentPath, '*.mp3'),
		path.join(piecescontentPath, '*.jpeg'),
		path.join(piecescontentPath, '*.JPEG')
	]).on('change', function(vinyl) {
		return gulp.src(path.normalize(vinyl))
		.pipe(gulp.dest(distPath))
		.pipe(gulp.dest(path.join('.tmp', fileFolder)));

	}).on('add', function(vinyl) {
		return gulp.src(path.normalize(vinyl))
		.pipe(gulp.dest(distPath))
		.pipe(gulp.dest(path.join('.tmp', fileFolder)));
	});

	var contentWatcher = watch([
		path.join(piecescontentPath, path.normalize('*/*')),
		path.join(piecescontentPath, path.normalize('**/**/*')),
		'!' + path.join(piecescontentPath, path.normalize('**/.DS_Store')),
		'!' + path.join(piecescontentPath, path.normalize('**/*.zip')),
		'!' + path.join(piecescontentPath, path.normalize('**/scss')),
		'!' + path.join(piecescontentPath, path.normalize('**/scss/*')),
		'!' + path.join(piecescontentPath, path.normalize('**/scss/**/*')),
		'!' + path.join(piecescontentPath, path.normalize('**/node_modules/*')),
		'!' + path.join(piecescontentPath, '*.mp4'),
		'!' + path.join(piecescontentPath, '*.jpg'),
		'!' + path.join(piecescontentPath, '*.JPG'),
		'!' + path.join(piecescontentPath, '*.png'),
		'!' + path.join(piecescontentPath, '*.mp3'),
		'!' + path.join(piecescontentPath, '*.jpeg'),
		'!' + path.join(piecescontentPath, '*.JPEG')
	]).on('change', function(vinyl) {
		if (vinyl.indexOf('.js') !== -1) {
			var folder = vinyl.split(piecescontentPath)[1].split(sep)[0];
			try {
				var conf = jsonfile.readFileSync('.' + sep + piecescontentPath + folder + sep + 'build.conf.json');
				return gulp.src( concatlibSrc(folder, conf.JS.libs, conf.JS.scripts ) )
					.pipe( concat(conf.JS.target) )
					.pipe(gulp.dest( path.join(distPath, folder, 'dist') ) );
			} catch(err) {
				console.log('Reloading JS without bundling')
			}
		}
		var changed = path.normalize(vinyl).split(piecescontentPath)[1];
		var dest = changed.substr(0, changed.lastIndexOf(sep));
		return gulp.src(path.normalize(vinyl))
			.pipe(gulp.dest(path.join(distPath, dest)));
	}).on('add', function(vinyl) {
		if (vinyl.indexOf('scss') === -1) {
			contentWatcher.add(vinyl);
			var changed = vinyl.split(piecescontentPath)[1];
			var distDest = distPath + changed.substr(0, changed.lastIndexOf(sep));
			return gulp.src(path.normalize(vinyl))
				.pipe(gulp.dest(distDest));
		}
	});
	var scssWatcher = watch([
		path.join(piecescontentPath, path.normalize('**/**/*.scss'))
	])
	.on('change', function(vinyl) {
		var split = vinyl.split(path.normalize('content/'));
		var folder = split[1].split(sep)[0];
		try {
			var conf = jsonfile.readFileSync( '.' + sep + 'src' + sep + 'content' + sep + folder + sep + 'build.conf.json' );
			vinyl = split[0] + 'content' + sep + folder + sep + path.normalize(conf.SCSS.source);
			compileSass(vinyl, conf.SCSS.target);
		} catch(err) {
			return gulp.src(".");
		}

	}).on('add', function(vinyl) {
		var split = vinyl.split(path.normalize('content/'));
		var folder = split[1].split(sep)[0];
		try {
			var conf = jsonfile.readFileSync( '.' + sep + 'src' + sep + 'content' + sep + folder + sep + 'build.conf.json' );
			scssWatcher.add(vinyl);
			vinyl = split[0] + path.normalize('content/') + folder + sep + path.normalize(conf.SCSS.source);
			compileSass(vinyl, conf.SCSS.target);
		} catch(err) {
			return gulp.src(".");
		}
	});
	var distWatcher = watch([
		path.join(distPath, path.normalize('**/*')),
		path.normalize('!**/*.scss')
	]).on('change', function(vinyl) {
		var dest = path.normalize(vinyl).split(distPath)[1];
		splitted = dest.split(sep);
		var folder = splitted[0];
		splitted[0] += '.zip';
		dest = '';
		splitted.forEach(function(val, index) { if (index < splitted.length - 1) dest+=val+sep; });
		try {
			var conf = jsonfile.readFileSync( '.' + sep + 'src' + sep + 'content' + sep + folder + sep + 'build.conf.json' );
			if (vinyl.indexOf(conf.SCSS.target) !== -1 || vinyl.indexOf(conf.JS.target) !== -1) {
				dest += 'dist/';
			}
		} catch(err) {
		}
		return gulp.src(path.normalize(vinyl))
				.pipe(gulp.dest(path.join('.tmp', contentFolder, dest)));
	}).on('add', function(vinyl) {
		distWatcher.add(vinyl);
		var changed = vinyl.split(distPath)[1];
		var servDest = path.normalize('.tmp/'+contentFolder+'/') + changed.split(sep)[0] + '.zip'
			+ changed.split( changed.split(sep)[0] )[1];
		servDest = servDest.substr( 0, servDest.lastIndexOf(sep) );
		return gulp.src(path.normalize(vinyl))
			.pipe(gulp.dest(servDest));
	});
});
gulp.task('build', ['check', 'clean', 'zip'], function() {
	console.log('Production build complete.');
});

/**
* Common tasks
*/
gulp.task('compileSass', folders(piecescontentPath, function(folder) {
	try {
		var conf = jsonfile.readFileSync( '.' + sep + 'src' + sep + 'content' + sep + folder + sep + 'build.conf.json' );
		return compileSass(path.join(piecescontentPath, folder, path.normalize(conf.SCSS.source)), conf.SCSS.target);
	} catch(err) {
		return gulp.src(".");
	}
}));
gulp.task('rawFiles', function() {
	return gulp.src([
				path.join(piecescontentPath, '*.mp4'),
				path.join(piecescontentPath, '*.jpg'),
				path.join(piecescontentPath, '*.JPG'),
				path.join(piecescontentPath, '*.png'),
				path.join(piecescontentPath, '*.mp3'),
				path.join(piecescontentPath, '*.jpeg'),
				path.join(piecescontentPath, '*.JPEG'),
			])
			.pipe(gulp.dest(distPath))
			.pipe(gulp.dest(path.join('.tmp', fileFolder)));
});

gulp.task('buildDist', folders(piecescontentPath, function(folder) {
	return gulp.src([
			path.join(piecescontentPath, folder, path.normalize('**/*')),
		])
		.pipe(gulp.dest(path.join(distPath, folder)));
}));

function concatlibSrc(folder /*arrays*/) {
	var res = [];
	for(var i = 1; i < arguments.length; i++) {
		for (var j = 0; j < arguments[i].length; j++) {
			res.push( '.' + sep + 'src' + sep + 'content' + sep + folder + sep + arguments[i][j] );
		}
	}
	return res;
}

gulp.task('buildContent', ['buildDist'], folders(piecescontentPath, function(folder) {
	try {
		var conf = jsonfile.readFileSync( '.' + sep + 'src' + sep + 'content' + sep + folder + sep + 'build.conf.json' );
		return gulp.src( concatlibSrc(folder, conf.JS.libs, conf.JS.scripts ) )
			.pipe( concat(conf.JS.target) )
			.pipe(gulp.dest( path.join(distPath, folder, 'dist') ) );
	} catch(err) {
		return gulp.src(".");
	}
}));

/**
* Dev specifics
*/
gulp.task('getFiles', ['rawFiles', 'compileSass', 'buildContent'], folders(distPath, function(folder) {
	return gulp.src(path.join(distPath, folder, path.normalize('**/*')))
			.pipe(gulp.dest(path.join('.tmp', contentFolder, folder + '.zip')));
}));
gulp.task('dev', ['getFiles'], function() {
	console.log('Dev build complete');
});

/**
* Prod specifics
*/
gulp.task('addPreviews', ['compileSass', 'buildContent', 'prodRawFiles'], folders(distPath, function(folder) {
	return gulp.src(path.join(previewsPath, folder)+'.png')
			.pipe(rename('preview.png'))
			.pipe(gulp.dest(path.join(distPath, folder)));
}));
gulp.task('prodRawFiles', ['rawFiles'], function() {
	return gulp.src([
				path.join(distPath, '*.mp4'),
				path.join(distPath, '*.jpg'),
				path.join(distPath, '*.png'),
				path.join(distPath, '*.mp3'),
				path.join(distPath, '*.jpeg'),
			])
			.pipe(gulp.dest(zippedPath));
});
gulp.task('zip', ['addPreviews'], folders(path.join(distPath), function(folder) {
	return gulp.src([
				path.join(distPath, folder, path.normalize('**/*')),
				'!' + path.join(distPath, '*.zip'),
				'!' + path.join(distPath, path.normalize('*.zip/*')),
			])
			.pipe(zip(folder + '.zip'))
			.pipe(gulp.dest(zippedPath));
}));

/**
*	Content Library build
*/
gulp.task('buildCL', function() {
	return execCLBuild();
});

function execCLBuild() {
	console.log(GREEN + 'Building Content Library... Please wait.' + NC);
	return run('cd '+ CLPath + ' && npm install > '+path.normalize('/dev/null')+' && gulp build && gulp compress && cd -').exec();
}

/**
* Slush
*/
/* @param patternArray [ { searchvalue: {string}, newvalue: {string} }, ... ] */
function slushGenFile(vinyl, dest, patternArray) {
	var fileName = vinyl.indexOf('index.html') !== -1 ? 'index.html' : vinyl;
	fileName = vinyl.indexOf('app.js') !== -1 ? 'app.js' : fileName;
	var tmp = fileName.split(sep);
	fileName = tmp.length > 0 ? tmp[tmp.length - 1] : fileName;
	var stream = gulp.src([vinyl]);
	if (patternArray) {
		if (patternArray.length) {
			for (var i = 0; i < patternArray.length; i++) {
				stream.pipe(replace(patternArray[i].searchvalue, patternArray[i].newvalue));
			}
		} else {
			stream.pipe(replace(patternArray.searchvalue, patternArray.newvalue));
		}
	}
	if (vinyl.indexOf('*') === -1)
		stream.pipe(rename(fileName));
	stream.pipe(gulp.dest(piecescontentPath+dest));
	return stream;
}
function slushGen(data) {
	data.type = 'custom';
	data.orientation = data.orientation ? data.orientation : 'horizontal';

	for (var i = 0; i < libs.length; i++) {
		if (libs[i].default) {
			slushGenFile(path.normalize(libs[i].path), path.normalize(data.folderName+'/js/vendor'));
		}
		if (data.libs.find(function(val) { return val === libs[i].lib })) {
			slushGenFile(path.normalize(libs[i].path), path.normalize(data.folderName+'/js/vendor'));
		}
	}

	slushGenFile(path.normalize('.slush/sets/'+data.type+'.index.html'),
		data.folderName,
		[
			{ searchvalue: '$(SLUSH_TITLE)', newvalue: data.folderName },
			{ searchvalue: '$(SLUSH_ORIENTATION)', newvalue: data.orientation === 'auto' ? '' : data.orientation },
		]);

	var appJs = data.libs.find(function(val){ return val === 'switchboard content library' }) ? path.normalize('.slush/sets/js/withsb.app.js') : path.normalize('.slush/sets/js/app.js');
	var hq = data.hq === 'other' ? data['manual-hq'] : data.hq;
	slushGenFile(path.normalize(appJs),
		path.normalize(data.folderName+'/js'),
		[
			{ searchvalue: '$(SLUSH_PROJECT_NAME)', newvalue: data.folderName },
			{ searchvalue: '$(SLUSH_AUTHOR)', newvalue: data.authorName },
			{ searchvalue: '$(SLUSH_EMAIL)', newvalue: data.authorEmail },
			{ searchvalue: '$(SLUSH_DATE)', newvalue: new Date() },
			{ searchvalue: '$(SLUCH_HQ)', newvalue: 'https://'+ hq +'/' },
			{ searchvalue: '$(SLUCH_ORIENTATION)', newvalue: data.orientation },
		]);

	slushGenFile(path.normalize('.slush/sets/js/helpers.js'), path.normalize(data.folderName+'/js'));

	slushGenFile(path.normalize('.slush/json/build.conf.json'), path.normalize(data.folderName));

	slushGenFile(path.normalize('.slush/sets/scss/**/*.scss'), path.normalize(data.folderName+'/scss'));

}

function letsSlush() {
	var questions = [
		{
			type: 'string',
			name: 'folderName',
			message: 'content name:',
			validate: function(input) {
				if (input.length > 0) {
					try {
						var stats = fs.statSync(piecescontentPath+input);
						return '[Error] Provided name is already assigned to an existing Content Asset';
					} catch (err) {
						return true;
					}
				} else {
					return '[Error] name is required to add content to the project';
				}
			}
		}, {
			type: 'string',
			name: 'authorName',
			message: 'author:',
			default: process.platform === 'win32' ? process.env.USERNAME : process.env.USER
		}, {
			type: 'string',
			name:'authorEmail',
			message: 'author email:',
			validate: function(input) {
				if (input.length === 0) {
					return true;
				} else {
					if (input.length > 5 && input.indexOf('@') > 0 && input.lastIndexOf('.') > input.indexOf('@')) {
						return true;
					} else {
						return 'Please provide a valid email address or leave blank.';
					}
				}
			}
		},/* {
			type: 'list',
			name: 'type',
			message: 'content type:',
			choices: ['custom', 'rotator']
		}, */{
			type: 'checkbox',
			name: 'libs',
			message: 'additional libraries: (content framework, jquery and lodash are included by default)',
			choices: [
				{
					name: 'socket-io-client.js (client socket lib for LMS)'
				}, {
					name: 'handlebars.js (templating library)'
				}, {
					name: 'BBCode-parser.js (BBCode parser library)'
				}, {
					name: 'moment.js (date and time library)'
				}
			],
			// when: function(answers) {
			// 	return answers.type === 'custom' ? true : false;
			// },
		}, {
			type: 'list',
			name: 'orientation',
			message: 'orientation:',
			choices: function(answers) {
				for (var i = 0; i < answers.libs.length; i++) {
					if (answers.libs[i] === 'switchboard content library') {
						return ['horizontal', 'vertical'/*, 'auto'*/];
					}
				}
				return ['horizontal', 'vertical'];
			},
			// when: function(answers) {
			// 	return answers.type === 'custom' ? true : false;
			// }
		}, {
			type: 'list',
			name: 'hq',
			message: 'hq:',
			choices: function() {
				var hqs = require('./.slush/json/hqs');
				for (var i = 0; i < hqs.length; i++) {
					if (hqs[i] === 'separator') {
						hqs[i] = new inquirer.Separator();
					}
				}
				return hqs;
			},
			when: function(answers) {
				if (!answers.libs) {
					return false;
				}
				for (var i = 0; i < answers.libs.length; i++) {
					if (answers.libs[i] === 'switchboard content library'/* && answers.type === 'custom'*/) {
						return true;
					}
				}
				return false;
			}
		}, {
			type: 'input',
			name: 'manual-hq',
			message: 'other hq: https://',
			when: function(answers) {
				return answers.hq === 'other' ? true : false;
			},
			validate: function(input) {
				if (input.length > 0) {
					if (input.indexOf('.') > 0) {
						if (input.lastIndexOf('/') === input.length - 1) {
							input.length--;
						}
						if (input.indexOf('http')) {
							if (input.indexOf('/') > 0) {
								input.slice(input.lastIndexOf('/') + 1, input.length);
							}
						}
						return true;
					}
				}
				return 'Please provide a valid email address or leave blank.';
			}
		},
	];
	return inquirer.prompt(questions).then(function (answers) {
		slushGen(answers);
		console.log(GREEN+'New Content Asset successfully created. Path: '+piecescontentPath+answers.folderName+sep+NC);
	});
}
gulp.task('slush', function() {
	console.log('\nThis utility will walk you through generating a new Content Asset for your project.\nGenerated content will be created in '+piecescontentPath+'\n\nDocumentation available at doc/slush.html\n\nPress ^C at any time to quit.');
	return letsSlush();
});

/**
* Server specifics
*/
gulp.task('switchboardenv', function() {
	return gulp.src([
		path.join(path.normalize('src/switchboard/js/**/*'))
	])
	.pipe(gulp.dest(path.normalize('.tmp/js/')));
});

gulp.task('cleanServ', function() {
	return del.sync('.tmp');
});

gulp.task('datasources', function() {
	return gulp.src(datasourcesPath + '*.csv')
		.pipe(gulp.dest('.tmp' + sep + 'datasources'));
});

gulp.task('serve', ['datasources', 'cleanServ', 'switchboardenv', 'dev'], function() {
	return browserSync({
		files: [
			path.join('.tmp', contentFolder, path.normalize('**/*')),
			path.normalize('!**/*.css')
		],
		server: {
			routes: {
				'/content': 'content',
				'/api/dataSource': 'datasources'
			},
			baseDir: '.tmp',
			directory: true,
		},
		startPath: 'content',
		port: 8080,
	});
});
