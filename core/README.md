# Install

When cloning this repository for the first time, run `sh install.sh` to initialise your local environment.

This script requires `Node.js` and `npm`. See below link for more information.

https://nodejs.org/en/download/package-manager/

# Quick Start

Run `gulp slush` to generate a new content asset for your project.

Run `gulp` to launch the dev environment. This will serve and watch all your source files, replicating Switchboard.

Run `gulp help` for help & documentation [work in progress]