#!/bin/bash

#############################################################
#
#	install.sh
#
#	@file	This script will initialise the Coates Digital
#	Productions development environment.
#
#	@author Chad <chad.vanet@coatesgroup.com>
#	@date	2016-11-28T03:32:41
#
#############################################################

BLUE='\033[0;34m'
CYAN='\033[0;36m'
NC='\033[0m'

function initSubmodule() {
	if [ ! -f ".gitmodules" ] || ! grep -q "\[submodule \".slush\"\]" .gitmodules
	then
		git submodule add git@gitlab.com:chad.vanet/dev-tools.git .slush # TODO: Init source directory in other location such as /usr/local/etc/ (?)
	fi

	git submodule init && git submodule update --remote
	/bin/cp -f ./.slush/core/gulpfile.js .
	/bin/cp -f ./.slush/core/bower.json .
	/bin/cp -f ./.slush/core/bowerrc.json .bowerrc
	/bin/cp -f ./.slush/core/package.json .
	/bin/cp -f ./.slush/core/README.md .
	/bin/cp -f ./.slush/core/gitignore ./.gitignore
	mkdir -p src/content src/datasources src/previews src/vendor/soda src/switchboard/js;
	/bin/cp -f ./.slush/vendor/Soda.js src/vendor/soda/
}

function dependencies() {
	echo "${CYAN}Verifying global dependencies...${NC}"
	if ! npm list -g gulp-cli; then
		npm install -g gulp-cli
	fi
	if ! npm list -g bower; then
		npm install -g bower
	fi
	echo "${CYAN}Global dependencies verified.${NC}"
}

function tips() {
	echo "\n${CYAN}Tip: Run ${BLUE}gulp slush${CYAN} to generate a new Content Asset.${NC}"
	echo "${CYAN}Tip: Run ${BLUE}gulp${CYAN} to launch your local environment.${NC}"
	echo "${CYAN}Tip: Run ${BLUE}gulp build${CYAN} to zip up all Content Assets for upload.${NC}"
}

echo "Configuring Coates Digital Productions Development Environment"

dependencies
initSubmodule

echo "${CYAN}Installing project dependencies...${NC}"
npm install
echo "${CYAN}Project dependencies installed.${NC}"

echo "${CYAN}Installing vendor libraries...${NC}"
bower install
echo "${CYAN}Vendor libraries installed.${NC}"

echo "${CYAN}Building Content Library...${NC}"
cd src/vendor/content-library && npm i && gulp build && gulp compress && cd -;
echo "${CYAN}Done.${NC}"


tips
